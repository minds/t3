import { NgModule } from "@angular/core";
import { T3TrustService } from "./trust.service";
import { T3TrustComponent } from "./trust.component";
import { T3VerificationModule } from "../verification/verification.module";
import { CommonModule } from "@angular/common";
import { T3TrustTraversalService } from "./traversal.service";

@NgModule({
  imports: [T3VerificationModule, CommonModule],
  providers: [T3TrustService, T3TrustTraversalService],
  declarations: [T3TrustComponent],
  entryComponents: [T3TrustComponent],
  exports: [T3TrustComponent]
})
export class T3TrustModule {}
