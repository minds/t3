import dids from "./trust.defaults";

const defaults: Map<string, any> = new Map();

for (const did of dids) {
  defaults.set(did.did.toLowerCase(), did);
}

export default defaults;
