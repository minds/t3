import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import { T3TrustService } from "./trust.service";
import { T3TrustTraversalService } from "./traversal.service";
import data from "./trust.defaults";

@Component({
  selector: "t3-trust",
  templateUrl: "./trust.component.html",
  styleUrls: ["trust.component.scss"],
  animations: [
    // Fade media in after load
    trigger("fadeAnimation", [
      state(
        "in",
        style({
          opacity: 1
        })
      ),
      state(
        "out",
        style({
          opacity: 0.3
        })
      ),
      transition("in <=> out", [animate("100ms")])
    ])
  ]
})
export class T3TrustComponent implements OnInit, OnDestroy {
  view: string = "unlock";
  actorDid: string = "000";
  showDetails: boolean = false;
  user: any = null;
  hasVotedOnUser: boolean = false;
  hasVotedOnUserThisSession: boolean = false;
  newScore: number;
  sliderTimeout: any = null;
  pagingTimeout: any = null;
  currentScore: number = 0;
  lastIndex: number;
  currentIndex: number = 0;
  hasRecalculated: boolean = false;
  isPaging: boolean = false;

  users;

  usersNew: Array<any> = []; //!!

  constructor(
    public trustService: T3TrustService,
    private traversalService: T3TrustTraversalService
  ) {}

  ngOnInit() {
    this.traversalService.fetchFromEthereum(
      "0x6afd852ac0a9bf29e2841453531bba322908732e"
    );

    this.traversalService.trusts$.subscribe(trusts => {
      this.users = trusts;
      //if (!this.user) {
      this.user = this.users[0];
      this.lastIndex = this.users.length - 1;
      //}
    });

    // this.actorDid = this.users[0].from_did;

    // this.user = this.users[0];
    this.currentIndex = 0;
    // this.hasVotedOnUser = this.user.vote ? true : false;
    // this.currentScore = this.user.score;

    this.lastIndex = this.users.length - 1;
  }

  async submitVote(vote) {
    await this.trustService.vote({
      did: this.user.entity_did,
      score: vote
    });
    switch (vote) {
      case -1:
        console.log("ur fake news");
        break;
      case 0:
        console.log("idk what to think");
        break;
      case 1:
        console.log("ur legit news");
    }
    this.hasVotedOnUser = true;
    this.hasVotedOnUserThisSession = true;
    this.user.score = vote;
    this.recalculateScore();
  }

  recalculateScore() {
    this.hasVotedOnUser = true;
    this.hasRecalculated = true;

    this.newScore = 1; // ! should be calculated
    this.user.score = this.newScore;

    if (this.sliderTimeout) {
      clearTimeout(this.sliderTimeout);
    }

    this.sliderTimeout = setTimeout(() => {
      this.hasRecalculated = false;
      this.currentScore = this.newScore;
    }, 700);
  }

  page(direction: number) {
    this.isPaging = true;
    this.currentScore = this.user.score;

    if (this.pagingTimeout) {
      clearTimeout(this.pagingTimeout);
    }

    if (this.sliderTimeout) {
      clearTimeout(this.sliderTimeout);
    }

    const previewIndex = this.currentIndex + direction;
    if (direction === 1) {
      this.currentIndex = previewIndex > this.lastIndex ? 0 : previewIndex;
    } else {
      this.currentIndex = previewIndex < 0 ? this.lastIndex : previewIndex;
    }

    this.user = this.users[this.currentIndex];
    this.newScore = this.user.score;
    this.hasVotedOnUser = this.user.vote ? true : false;

    this.pagingTimeout = setTimeout(() => {
      this.isPaging = false;
    }, 100);

    this.sliderTimeout = setTimeout(() => {
      this.hasRecalculated = false;
      this.currentScore = this.newScore;
    }, 700);
  }

  ngOnDestroy() {
    if (this.sliderTimeout) {
      clearTimeout(this.sliderTimeout);
    }
    if (this.pagingTimeout) {
      clearTimeout(this.pagingTimeout);
    }
  }

  // -------------------------------------------------------------------------------------------
}
