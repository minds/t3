import { Injectable, Inject } from "@angular/core";
import { Observable } from "rxjs";
import { WEB3 } from "../web3/web3.module";
import * as TrustContract from "../web3/contracts/trust.contract";
import { HttpClient } from "@angular/common/http";
import { uPortVerificationService } from "../uport/verification.service";
import { first } from "rxjs/operators";

const T3_SERVER_URI = "https://t3-backend.ngrok.io";

@Injectable()
export class T3TrustService {
  constructor(
    private verificationService: uPortVerificationService,
    private http: HttpClient
  ) {}

  async vote(obj) {
    const profile = await this.verificationService.profile$
      .pipe(first())
      .toPromise();
    this.http
      .post(T3_SERVER_URI + "/vote", {
        profile,
        to: obj.did.split(":")[2],
        score: obj.score
      })
      .subscribe(data => {});
  }

  getScore(user) {
    // let score = 0.5;
    // switch (parseInt(user.score)) {
    //   case 1:
    //     score = 1 / user.degree;
    //     break;
    //   case 0:
    //     break;
    //   case -1:
    //     score = 0.01 * user.degree;
    //     break;
    // }
    let score = user.score / user.degree;
    if (score > 1) {
      score = 1;
    }
    if (score < 0) {
      score = 0;
    }
    return score;
  }
}
