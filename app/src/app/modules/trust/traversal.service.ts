import { Injectable, Inject } from "@angular/core";
import { WEB3 } from "../web3/web3.module";
import * as TrustContract from "../web3/contracts/trust.contract";
import { BehaviorSubject } from "rxjs";
import defaults from "./trust.map";

@Injectable()
export class T3TrustTraversalService {
  from: string;
  trustsRaw$: BehaviorSubject<any[]> = new BehaviorSubject([]);
  trusts$ = this.trustsRaw$.pipe();
  trustMap: Map<string, any> = new Map();

  constructor(@Inject(WEB3) private web3) {}

  async fetchFromEthereum(from: string, degree = 1) {
    const contract = new this.web3.eth.Contract(
      TrustContract.ABI,
      TrustContract.ADDRESS
    );
    const trusts = await contract.methods.getTrusts(from).call();

    for (const trust of trusts) {
      const entity_did = "did:ethr:" + trust.from.toLowerCase();
      const actor_did = "did:ethr:" + trust.to.toLowerCase();
      let score = trust.score;
      const { name, avatar } = defaults.get(actor_did);
      if (this.trustMap.has(actor_did)) {
        const current = this.trustMap.get(actor_did);
        if (score > 0) {
          score = current.score + score / current.degree;
        }
        if (score < 0) {
          score = current.score - score / current.degree;
        }
      }
      this.trustMap.set(actor_did, {
        entity_did,
        actor_did,
        score,
        degree,
        name,
        avatar
      });
      this.fetchFromEthereum(trust.to, degree + 1);
    }
    this.trustsRaw$.next(Array.from(this.trustMap.values()));
  }
}
