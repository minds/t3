export default [
  {
    name: "Homer Simpson",
    avatar: "/assets/homer.png",
    did: "did:ethr:0x3e010d48eee73c9f545591c52e3bd23a7340a91f"
  },
  {
    name: "Marge Simpson",
    avatar: "/assets/marge.png",
    did: "did:ethr:0x004aacab4da0dae5220fb3e5b99e41097f8c9417"
  },
  {
    name: "Bart Simpson",
    avatar: "/assets/bart.png",
    did: "did:ethr:0x3b983ef216efc937718a0aa085b4f2ffce643132"
  },
  {
    name: "Lisa Simpson",
    avatar: "/assets/lisa.png",
    did: "did:ethr:0x92e051b3527718fae161e88b495e3ef75b4e48d1"
  },
  {
    name: "Maggie Simpson",
    avatar: "/assets/maggie.png",
    did: "did:ethr:0x37b79eaa6fc33519de02b58083231b6be26dbc40"
  },
  {
    name: "Santa's Little Helper",
    avatar: "/assets/santasLittleHelper.png",
    did: "did:ethr:0x41a032e4e6e19d68614cadc8fd23bd437c23450b"
  },
  {
    name: "Snowball II/V",
    avatar: "/assets/snowball.png",
    did: "did:ethr:0x5685a04f8524220860669128a11d4aed6cdf529e"
  },
  {
    name: "Abraham Simpson",
    avatar: "/assets/Grampa.png",
    did: "did:ethr:0xb979ab1d64f850249641af7599662321461041b3"
  },
  {
    name: "Apu Nahasapeemapetilon",
    avatar: "/assets/apu.png",
    did: "did:ethr:0xf01f4e54a5d9bb53d0cbe955899eb3ad348fab01"
  },
  {
    name: "Barney Gumble",
    avatar: "/assets/barney.png",
    did: "did:ethr:0x91eb2563f8d0d359b5e2ff3922a3f4ec69d37b26"
  },
  {
    name: "Bleeding Gums Murphy",
    avatar: "/assets/bleedingGums.png",
    did: "did:ethr:0x06a9f2ca4ce800b614e171da3bde8b5a29f0fd89"
  },
  {
    name: "Chief Clancy Wiggum",
    avatar: "/assets/chief.png",
    did: "did:ethr:0xff3d65b2dc6f356f00ad36dd44d25107b63bc9fb"
  },
  {
    name: "Dewey Largo",
    avatar: "/assets/deweyLargo.png",
    did: "did:ethr:0x1935ee777be794bbc26b3b4bbcc6951922fc04af"
  },
  {
    name: "Eddie",
    avatar: "/assets/eddie.png",
    did: "did:ethr:0x7a6729affaaedb24c68028192287fb549f305575"
  },
  {
    name: "Edna Krabappel",
    avatar: "/assets/edna.png",
    did: "did:ethr:0x1af1a2dfc69c477542d816e76158bd6c79801269"
  },
  {
    name: "Itchy",
    avatar: "/assets/itchy.png",
    did: "did:ethr:0x60409600114557488900d39f62d6f477bb405eeb"
  },
  {
    name: "Janey Powell",
    avatar: "/assets/janey.png",
    did: "did:ethr:0x6109a92979a1d0126389f8da00681f30a3ac544e"
  },
  {
    name: "Jasper Beardsley",
    avatar: "/assets/beardsley.png",
    did: "did:ethr:0xea9a12928547c33d27ee435f5ca2b9582beaa66b"
  },
  {
    name: "Kent Brockman",
    avatar: "/assets/brockman.png",
    did: "did:ethr:0xfa4f44357ef93909bb1697915cd39afbb6259db8"
  },
  {
    name: "Krusty The Clown",
    avatar: "/assets/krusty.png",
    did: "did:ethr:0xa1e7fbb91122cbe80afa288b25aaa615e1d8fd38"
  },
  {
    name: "Lenny Leonard",
    avatar: "/assets/lenny.png",
    did: "did:ethr:0x8d6e0709f6677b66f6f82a0225f507542d29f5d6"
  },
  {
    name: "Lou",
    avatar: "/assets/lou.png",
    did: "did:ethr:0x0ef0c4f2b0c1ed96ac8efa3eb4c25b8f54e99e63"
  },
  {
    name: "Martin Prince",
    avatar: "/assets/martin.png",
    did: "did:ethr:0x0f0f5587637660f966797db8f72dbe5d99560768"
  },
  {
    name: "Marvin Monroe",
    avatar: "/assets/marvin.png",
    did: "did:ethr:0xc691eb3c4e5cee6ffdb954ade14aedf29ff747e6"
  },
  {
    name: "Milhouse Van Houten",
    avatar: "/assets/milhouse.png",
    did: "did:ethr:0x37502a09751ae6fe827a166db4ba0805812bf360"
  },
  {
    name: "Moe Szyslak",
    avatar: "/assets/moe.png",
    did: "did:ethr:0x06846a4e3879374d8a28b19fb680750ba8def2a7"
  },
  {
    name: "Mr. Burns",
    avatar: "/assets/burns.png",
    did: "did:ethr:0xa163ff521e47bffcca12b87274db7db4bb8cfbbc"
  },
  {
    name: "Ned Flanders",
    avatar: "/assets/flanders.png",
    did: "did:ethr:0xa0a872594d34efaf1cc15979cef1fd7945989357"
  },
  {
    name: "Otto Mann",
    avatar: "/assets/otto.png",
    did: "did:ethr:0xfb35d6a595698d11b2edc74be285b137da862f2a"
  },
  {
    name: "Patty Bouvier",
    avatar: "/assets/patty.png",
    did: "did:ethr:0xa66d1dcbae93f0f8a8492fea2cad52c21f5c863d"
  },
  {
    name: "Ralph Wiggum",
    avatar: "/assets/ralph.png",
    did: "did:ethr:0xe436aad18c8473cee19f4a290afb00fd81eac5eb"
  },
  {
    name: "Reverend Timothy Lovejoy",
    avatar: "/assets/lovejoy.png",
    did: "did:ethr:0x848434682c78f0391641b4575f5734888608796d"
  },
  {
    name: "Scratchy",
    avatar: "/assets/scratchy.png",
    did: "did:ethr:0x85d24de072b295a4552e302e55b466b7cadad69c"
  },
  {
    name: "Selma Bouvier",
    avatar: "/assets/selma.png",
    did: "did:ethr:0x36e3a6e723fecdd28f8aee5b18dd110f54e411aa"
  },
  {
    name: "Seymour Skinner",
    avatar: "/assets/skinner.png",
    did: "did:ethr:0x0210c3a8c0be047b65cb16b12c3e65f682556585"
  },
  {
    name: "Sherri",
    avatar: "/assets/sherri.png",
    did: "did:ethr:0xb761469b6093edfeb5bf7515e929750f52be8650"
  },
  {
    name: "Terri",
    avatar: "/assets/terri.png",
    did: "did:ethr:0x19574d2965ed5fb9277f26d313543ec49f1e580c"
  },
  {
    name: "Sideshow Bob",
    avatar: "/assets/sideshowBob.png",
    did: "did:ethr:0x555b21efaa2ee3ca666f8e5d10938b07d2ef5dff"
  }
];
