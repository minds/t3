import { NgModule, Inject } from "@angular/core";
import { uPortVerificationService } from "./verification.service";
import { Web3Module } from "../web3/web3.module";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { DomSanitizer } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";

@NgModule({
  imports: [Web3Module, HttpClientModule, CommonModule],
  providers: [
    {
      provide: uPortVerificationService,
      useFactory: (http: HttpClient, domSanitizer: DomSanitizer) => {
        return new uPortVerificationService(http, domSanitizer);
      },
      deps: [HttpClient, DomSanitizer]
    }
  ]
})
export class uPortModule {}
