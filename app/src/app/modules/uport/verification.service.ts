import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { SafeUrl, DomSanitizer } from "@angular/platform-browser";
import { Subject, interval, BehaviorSubject, Observable } from "rxjs";
import { first } from "rxjs/operators";
import { WEB3 } from "../web3/web3.module";
import Web3 from "web3";
import EthrDID from "ethr-did";
import * as TrustContract from "../web3/contracts/trust.contract";
import * as DIDContract from "../web3/contracts/did.contract";
import { transport, message, crypto } from "uport-transports";

declare const Connect: any;

const T3_SERVER_URI = "https://t3-backend.ngrok.io";

@Injectable()
export class uPortVerificationService {
  qrImageUri$: Subject<SafeUrl> = new Subject();
  uPortLink$: Subject<SafeUrl> = new Subject();
  data$: BehaviorSubject<any> = new BehaviorSubject(null);
  polling$: Observable<number> = interval(5000);
  profile$: BehaviorSubject<string> = new BehaviorSubject(null);
  did$: BehaviorSubject<string> = new BehaviorSubject(null);
  addingDelegate$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {
    this.start();
  }

  /**
   * Commence login
   */
  async start(): Promise<void> {
    await this.fetch();
    this.startPolling();
  }

  async fetch(): Promise<void> {
    const data = await this.http.get(T3_SERVER_URI + "/unlock").toPromise();
    this.data$.next(data);
  }

  /**
   * Fetch a qr code
   */
  fetchQrImageUri(): void {
    this.data$.subscribe((data: any) => {
      if (!data) return;
      this.qrImageUri$.next(
        this.sanitizer.bypassSecurityTrustUrl(data.qrImageData)
      );
    });
  }

  fetchUPortLink(): void {
    this.data$.subscribe((data: any) => {
      if (!data) return;
      const transportQR = transport.qr.send();
      transportQR(data.requestToken);
    });
  }

  startPolling() {
    const s = this.polling$.subscribe(async () => {
      console.log("polling");
      const data = await this.data$.pipe(first()).toPromise();
      if (!data || !data.sessionId) {
        console.log("no data");
        return;
      }
      this.http
        .post(T3_SERVER_URI + "/poll/unlock", {
          sessionId: data.sessionId
        })
        .subscribe((data: any) => {
          if (data.status === "ok") {
            this.profile$.next(data.profile);
            this.did$.next(data.profile.did);
            transport.ui.close(); // close modal
            //this.addDelegate();
            s.unsubscribe();
          }
        });
    });
  }

  async addDelegate() {
    console.log("getting currently set did");
    const did = await this.did$.pipe(first()).toPromise();
    const address = did.split(":")[2];
    console.log(did, address);
    const uport = new Connect("Minds T3");
    const provider = uport.getProvider();
    const web3 = new Web3(provider);

    const contract = new web3.eth.Contract(
      DIDContract.ABI,
      DIDContract.ADDRESS
    );
    const tx1 = contract.methods
      .addDelegate(
        "0x6afd852ac0a9bf29e2841453531bba322908732e",
        web3.utils.fromAscii("DID-JWT"),
        address,
        86400 * 365 // 1 year
      )
      .encodeABI();
    console.log(await uport.requestPersonalSign(tx1));
    // // contract.methods.issueTrustViaDelegate(address, address, 1);
    // // console.log(tx1);
    // const signed = await web3.eth.sendTransaction(
    //   {
    //     to: DIDContract.ADDRESS,
    //     from: address,
    //     data: tx1
    //   },
    //   address
    // );
    // console.log(contract, tx1, signed);

    const tx = {
      to: TrustContract.ADDRESS,
      data: {}
    };

    //web3.sign;

    // const ethrDid = new EthrDID({
    //   provider: web3.currentProvider,
    //   address: "0xd21b90f056c9e42957da86117f0aa6b821418da4"
    // });
    // this.addingDelegate$.next(true);
    // await ethrDid.addDelegate(TrustContract.ADDRESS);
    // this.addingDelegate$.next(false);
  }
}
