import { NgModule, InjectionToken } from "@angular/core";
import Web3 from "web3";

export const WEB3 = new InjectionToken<Web3>("web3");

@NgModule({
  providers: [
    {
      provide: WEB3,
      useFactory: () => {
        try {
          const provider =
            "ethereum" in window ? window["ethereum"] : Web3.givenProvider;
          return new Web3(provider);
        } catch (err) {
          throw new Error(
            "Non-Ethereum browser detected. You should consider trying Mist or MetaMask!"
          );
        }
      }
    }
  ]
})
export class Web3Module {}
