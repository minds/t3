import { NgModule } from '@angular/core';
import { T3VerificationComponent } from './verification.component';
import { uPortModule } from '../uport/uport.module';
import { Router, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    uPortModule,
    RouterModule,
    RouterModule.forChild([
      {
        path: 'verification',
        component: T3VerificationComponent
      }
    ])
  ],
  declarations: [T3VerificationComponent],
  entryComponents: [T3VerificationComponent],
  exports: [T3VerificationComponent]
})
export class T3VerificationModule {}
