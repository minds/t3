import { Component } from "@angular/core";
import { uPortVerificationService } from "../uport/verification.service";

declare const Connect: any;
@Component({
  selector: "t3-verification",
  templateUrl: "./verification.component.html"
})
export class T3VerificationComponent {
  constructor(public verificationService: uPortVerificationService) {}

  ngOnInit() {
    this.verificationService.fetchQrImageUri();
    this.verificationService.fetchUPortLink();
    this.verificationService.did$.subscribe(did => {
      // if (did) alert("verified via uport");
    });
  }
}
