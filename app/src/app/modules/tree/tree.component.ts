import { Component, ViewChild } from "@angular/core";

declare const cytoscape: any;

@Component({
  selector: "t3-tree",
  templateUrl: "./tree.component.html"
})
export class T3TreeComponent {
  cy;

  @ViewChild("cyptoscape", { static: false }) element;
  ngOnInit() {}
}
