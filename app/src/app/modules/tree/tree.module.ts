import { NgModule } from "@angular/core";
import { T3TreeComponent } from "./tree.component";

@NgModule({
  declarations: [T3TreeComponent],
  exports: [T3TreeComponent]
})
export class T3TreeModule {}
