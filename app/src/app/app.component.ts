import { Component } from "@angular/core";
import { uPortVerificationService } from "./modules/uport/verification.service";

declare const Connect: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "t3";
  view: string = "unlock"; // ! TODO later: this should be 'unlock'

  constructor(public verificationService: uPortVerificationService) {}

  switchView(view) {
    this.view = view;
    // do something with router or url?
  }
}
