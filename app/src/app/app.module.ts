import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { uPortModule } from "./modules/uport/uport.module";
import { T3VerificationModule } from "./modules/verification/verification.module";
import { T3TrustModule } from "./modules/trust/trust.module";
import { T3TreeModule } from "./modules/tree/tree.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    uPortModule,
    T3VerificationModule,
    T3TrustModule,
    T3TreeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
