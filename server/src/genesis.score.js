const { Credentials } = require("uport-credentials");
const fs = require("fs");
const Web3 = require("web3");
const Tx = require("ethereumjs-tx").Transaction;
const identity = JSON.parse(fs.readFileSync("./identity.json"));

const from = identity.did.split(":")[2];

const simpsons = JSON.parse(fs.readFileSync("./simpsons.json"));

const web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://rinkeby.infura.io/v3/dcaaa4b596d34bd1867a62bb916a0b31"
  )
);

const sendVotes = async (actor, user, score) => {
  const from = actor.keypair.did.split(":")[2];
  const to = user.keypair.did.split(":")[2];
  console.log("from " + from);
  privateKey = Buffer.from(actor.keypair.privateKey, "hex");
  const count = await web3.eth.getTransactionCount(from);
  const balance = await web3.eth.getBalance(from);

  const contract = new web3.eth.Contract(
    require("./contracts/trust").ABI,
    require("./contracts/trust").ADDRESS
  );

  const ADDRESS = require("./contracts/trust").ADDRESS;

  // console.log(count, balance);
  var rawTransaction = {
    chainId: 4,
    from,
    gasLimit: web3.utils.toHex(225000),
    gasPrice: web3.utils.toHex(10e8), // 1 Gwei
    to: ADDRESS,
    data: contract.methods
      .issueTrust(web3.utils.toHex(to), web3.utils.toHex(score))
      .encodeABI(),
    nonce: count
  };

  var tx = new Tx(rawTransaction, { chain: "rinkeby" });
  tx.sign(privateKey);
  const serializedTx = tx.serialize().toString("hex");
  console.log(rawTransaction, serializedTx);
  await web3.eth.sendSignedTransaction("0x" + serializedTx);
};

findCharacter = name => {
  for (const character of simpsons) {
    console.log(character.name, name);
    if (character.name === name) {
      return character;
    }
  }
};

const actor = { keypair: identity };
const user = findCharacter(process.argv[3]);
const score = process.argv[4];

console.log(`will give ${user} a score of ${score}`);

sendVotes(actor, user, score);
