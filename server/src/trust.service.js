const express = require("express");
const bodyParser = require("body-parser");
const ngrok = require("ngrok");
const decodeJWT = require("did-jwt").decodeJWT;
const { Credentials } = require("uport-credentials");
const transports = require("uport-transports").transport;
const message = require("uport-transports").message.util;
const fs = require("fs");
const crypto = require("crypto");
const identity = JSON.parse(fs.readFileSync("./identity.json"));
const EthrDID = require("ethr-did");
const HttpProvider = require("ethjs-provider-http");
const Web3 = require("web3");

let endpoint = "";
const TRUST_CONTRACT_ADDRESS = "0x619bebd9d803b4f0796c1dd0aece1e9fcadd55d1";
const DID_CONTRACT_ADDRESS = "0x6afd852ac0a9bf29e2841453531bba322908732e";

const web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://rinkeby.infura.io/v3/dcaaa4b596d34bd1867a62bb916a0b31"
  )
);

const app = express();
app.use(bodyParser.json({ type: "*/*" }));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");

  next();
});

//setup Credentials object with newly created application identity.
const credentials = new Credentials({
  ...{ appName: "Minds t3" },
  ...identity
});
const provider = new HttpProvider(
  "https://rinkeby.infura.io/v3/dcaaa4b596d34bd1867a62bb916a0b31"
);

const authenticatedRequests = new Map();

/**
 * Collect a QR code and return to the client
 */
app.get("/unlock", async (req, res) => {
  try {
    const sessionId = crypto.randomBytes(64).toString("hex");

    const requestToken = await credentials.createDisclosureRequest({
      requested: ["name", "avatar"],
      notifications: true,
      accountType: "keypair",
      network_id: "0x4",
      callbackUrl: endpoint + "/callback/unlock?sessionId=" + sessionId
    });

    res.send({
      requestToken,
      sessionId
    });
  } catch (err) {}
});

app.post("/vote", async (req, res) => {
  const to = req.body.to;
  const score = req.body.score;
  const profile = req.body.profile;
  const from = req.body.profile.did.split(":")[2];
  const push = transports.push.send(profile.pushToken, profile.boxPub);

  const txObject = {
    to: TRUST_CONTRACT_ADDRESS,
    net: "0x4",
    value: null,
    fn:
      "issueTrustViaDelegate(address " +
      from +
      ", address " +
      to +
      ", int8 " +
      score +
      ")"
  };

  const token = await credentials.createTxRequest(txObject, {
    callbackUrl: `${endpoint}/callback/vote`,
    callback_type: "post",
    networkId: "0x4"
  });
  console.log(token);
  push(token);

  res.send({
    token
  });
});

// uPort will call this endpoint directly
app.post("/callback/unlock", async (req, res) => {
  const jwt = req.body.access_token;
  const sessionId = req.query.sessionId;
  console.log(`[callback/unlock]: ${sessionId} `);
  try {
    const profile = await credentials.authenticateDisclosureResponse(jwt);

    const identityAddress = profile.did.split(":")[2]; // Nasty
    const delegateAddress = identity.did.split(":")[2]; // Nastier

    console.log("[credentials]: ", profile);
    authenticatedRequests.set(sessionId, profile);
    res.send({
      status: "success"
    });
  } catch (err) {
    console.log(err);
    res.send({
      status: "error",
      message: err.toString(),
      jwt
    });
  }
});

app.post("/callback/vote", async (req, res) => {
  console.log("vote callback received", req);
});

app.post("/poll/unlock", async (req, res) => {
  const sessionId = req.body.sessionId;
  console.log(`[poll/unlock]: ${sessionId} `);
  const profile = authenticatedRequests.get(sessionId.toString());

  if (profile) {
    console.log(`[poll/unlock]: ${sessionId}: success`);
    res.send({
      status: "ok",
      profile
    });
  } else {
    res.send({
      status: "error",
      message: "Not yet authenticated"
    });
  }
});

// run the app server and tunneling service
const server = app.listen(8088, () => {
  ngrok
    .connect({
      proto: "http",
      subdomain: "t3-backend",
      addr: 8088
    })
    .then(ngrokUrl => {
      endpoint = ngrokUrl;
      console.log(`Login Service running, open at ${endpoint}`);
    });
});
