const { Credentials } = require("uport-credentials");
const fs = require("fs");
const Web3 = require("web3");
const Tx = require("ethereumjs-tx").Transaction;
const identity = JSON.parse(fs.readFileSync("./identity.json"));

const from = identity.did.split(":")[2];

const simpsons = JSON.parse(fs.readFileSync("./simpsons.json"));

const web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://rinkeby.infura.io/v3/dcaaa4b596d34bd1867a62bb916a0b31"
  )
);

const get = async actor => {
  const address = actor.keypair.did.split(":")[2];
  const contract = new web3.eth.Contract(
    require("./contracts/trust").ABI,
    require("./contracts/trust").ADDRESS
  );

  console.log(await contract.methods.getTrusts(address).call());
};

findCharacter = name => {
  for (const character of simpsons) {
    if (character.name === name) {
      return character;
    }
  }
};

const actor = findCharacter(process.argv[2]);

console.log(`${actor.name} has the following trust scores`);

get(actor);
