const { Credentials } = require("uport-credentials");
const fs = require("fs");

const simpsons = [
  { name: "Homer Simpson" },
  { name: "Marge Simpson" },
  { name: "Bart Simpson" },
  { name: "Lisa Simpson" },
  { name: "Maggie Simpson" },
  { name: "Santa's Little Helper" },
  { name: "Snowball II/V" },
  { name: "Abraham Simpson" },
  { name: "Apu Nahasapeemapetilon" },
  { name: "Barney Gumble" },
  { name: "Bleeding Gums Murphy[B]" },
  { name: "Chief Clancy Wiggum" },
  { name: "Dewey Largo" },
  { name: "Eddie" },
  { name: "Edna Krabappel" },
  { name: "Itchy" },
  { name: "Janey Powell" },
  { name: "Jasper Beardsley" },
  { name: "Kent Brockman" },
  { name: "Krusty The Clown" },
  { name: "Lenny Leonard" },
  { name: "Lou" },
  { name: "Martin Prince" },
  { name: "Marvin Monroe[C]" },
  { name: "Milhouse Van Houten" },
  { name: "Moe Szyslak" },
  { name: "Mr. Burns" },
  { name: "Ned Flanders" },
  { name: "Otto Mann" },
  { name: "Patty Bouvier" },
  { name: "Ralph Wiggum" },
  { name: "Reverend Timothy Lovejoy" },
  { name: "Scratchy" },
  { name: "Selma Bouvier" },
  { name: "Seymour Skinner" },
  { name: "Sherri" },
  { name: "Terri" },
  { name: "Sideshow Bob" }
];

for (let i in simpsons) {
  const character = simpsons[i];
  const keypair = Credentials.createIdentity();
  simpsons[i] = { ...character, keypair };
}

fs.writeFileSync("./simpsons.json", JSON.stringify(simpsons, null, 2));
