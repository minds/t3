const { Credentials } = require("uport-credentials");
const fs = require("fs");
const Web3 = require("web3");
const Tx = require("ethereumjs-tx").Transaction;
const identity = JSON.parse(fs.readFileSync("./identity.json"));

const from = identity.did.split(":")[2];

const simpsons = JSON.parse(fs.readFileSync("./simpsons.json"));

const web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://rinkeby.infura.io/v3/dcaaa4b596d34bd1867a62bb916a0b31"
  )
);

const sendFunds = async address => {
  console.log("from " + from);
  privateKey = Buffer.from(identity.privateKey, "hex");
  const count = await web3.eth.getTransactionCount(from);
  const balance = await web3.eth.getBalance(from);
  console.log(count, balance);
  var rawTransaction = {
    chainId: 4,
    from,
    gasLimit: web3.utils.toHex(25000),
    gasPrice: web3.utils.toHex(10e8), // 1 Gwei
    to: address,
    value: web3.utils.toHex("50000000000000000"),
    // data: contract.methods.transfer(toAddress, amount).encodeABI(),
    nonce: count
  };

  var tx = new Tx(rawTransaction, { chain: "rinkeby" });
  tx.sign(privateKey);
  const serializedTx = tx.serialize().toString("hex");
  console.log(rawTransaction, serializedTx);
  await web3.eth.sendSignedTransaction("0x" + serializedTx);
  //.on("transactionHash", console.log);
};

(async () => {
  for (const character of simpsons) {
    await sendFunds(character.keypair.did.split(":")[2]);
  }
})();
