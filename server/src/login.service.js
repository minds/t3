const express = require("express");
const bodyParser = require("body-parser");
const ngrok = require("ngrok");
const decodeJWT = require("did-jwt").decodeJWT;
const { Credentials } = require("uport-credentials");
const transports = require("uport-transports").transport;
const message = require("uport-transports").message.util;
const fs = require("fs");
const identity = JSON.parse(fs.readFileSync("./identity.json"));

let endpoint = "http://07b7a55f.ngrok.io";
const app = express();
app.use(bodyParser.json({ type: "*/*" }));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");

  next();
});

//setup Credentials object with newly created application identity.
const credentials = new Credentials({
  ...{ appName: "Minds t3" },
  ...identity
});

const authenticatedRequests = new Map();

/**
 * Collect a QR code and return to the client
 */
app.get("/qr", async (req, res) => {
  try {
    const sessionId = Date.now();
    const requestToken = await credentials.createDisclosureRequest({
      requested: ["name"],
      notifications: true,
      callbackUrl: endpoint + "/callback?session=" + sessionId
    });
    console.log(decodeJWT(requestToken)); //log request token to console
    const uri = message.paramsToQueryString(
      message.messageToURI(requestToken),
      { callback_type: "post" }
    );
    const qr = transports.ui.getImageDataURI(uri);
    res.send({
      token: requestToken,
      qrImageData: qr,
      url: uri,
      sessionId
    });
  } catch (err) {}
});

app.post("/callback", async (req, res) => {
  const jwt = req.body.access_token;
  const sessionId = req.query.session;
  console.log(sessionId);
  try {
    const profile = await credentials.authenticateDisclosureResponse(jwt);
    console.log("SUCCESS");
    console.log("[credentials]: ", profile);
    authenticatedRequests.set(sessionId, profile);
    res.send({
      status: "success"
    });
  } catch (err) {
    console.log(err);
    res.send({
      status: "error",
      message: err.toString(),
      jwt
    });
  }
});

app.post("/poll-auth", async (req, res) => {
  const sessionId = req.body.session;
  console.log("polling request received for " + sessionId);
  const profile = authenticatedRequests.get(sessionId.toString());
  console.log(profile);
  if (profile) {
    res.send({
      status: "ok",
      profile
    });
  } else {
    res.send({
      status: "error",
      message: "Not yet authenticated"
    });
  }
});

// run the app server and tunneling service
const server = app.listen(8088, () => {
  ngrok
    .connect({
      proto: "http",
      subdomain: "t3-backend",
      addr: 8088
    })
    .then(ngrokUrl => {
      endpoint = ngrokUrl;
      console.log(`Login Service running, open at ${endpoint}`);
    });
});
