pragma experimental ABIEncoderV2;

import '../node_modules/ethr-did-registry/contracts/EthereumDIDRegistry.sol';

contract Trust {

  EthereumDIDRegistry didRegistry;

  struct TrustScore {
    address from;
    address to;
    int8 score;
  }

  mapping(address => TrustScore[]) public scores;
  // mapping(address => mapping(address => TrustScore)) public scores;

  constructor(address didRegistryAddress) public {
    didRegistry = EthereumDIDRegistry(didRegistryAddress);
  }

  // Test function REMOVE THIS
  function sayHello() public view returns (string) {
    return "hello";
  }

  function issueTrustViaDelegate(address from, address to, int8 score) public returns (bool) {
    bool validated = didRegistry.validDelegate(from, 'DID-JWT', msg.sender);
    require(validated);
    _issueTrust(from, to, score);
  }

  function issueTrust(address to, int8 score) {
    require(didRegistry.identityOwner(msg.sender) == msg.sender);
    _issueTrust(msg.sender, to, score);
  }

  /**
   * Issue trust (internal)
   * @param from address
   * @param to address
   * @param score int8
   * @return bool
   */
  function _issueTrust(address from, address to, int8 score) internal returns (bool) {
    TrustScore memory _trustScore = TrustScore(
      from,
      to,
      score
    );

    // scores[from][to] = _trustScore;
    scores[from].push(_trustScore);

    return true;
  }

  function getTrust(address from, address to) public view returns (int8) {
    for (uint index=0; index < scores[from].length; index++) {
      if (scores[from][index].to == to) {
        return scores[from][index].score;
      }
    }
    return 0;
  }

  function getTrusts(address from) public view returns (TrustScore[]) {
    return scores[from];
  }

}