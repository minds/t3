const Trust = artifacts.require("Trust");
const EthereumDIDRegistry = artifacts.require("EthereumDIDRegistry");

contract("Trust", accounts => {
  let trust;
  let didRegistry;

  beforeEach(async () => {
    didRegistry = await EthereumDIDRegistry.new();
    trust = await Trust.new(didRegistry.address);
  });

  it("should say hello", async () => {
    const say = await trust.sayHello.call();
    assert.equal(say, "hello");
  });

  it("should issue trust score", async () => {
    await trust.issueTrust(accounts[2], 1);
    assert.equal(await trust.getTrust(accounts[0], accounts[2]), 1);
  });

  it("should issue a negative trust score", async () => {
    await trust.issueTrust(accounts[3], -1);
    assert.equal(await trust.getTrust(accounts[0], accounts[3]), -1);
  });

  it("should issue multiple trust scores", async () => {
    await trust.issueTrust(accounts[2], 1);
    await trust.issueTrust(accounts[3], 1);
    await trust.issueTrust(accounts[4], -1);
    await trust.issueTrust(accounts[5], -1);
    assert.equal(await trust.getTrust(accounts[0], accounts[2]), 1);
    assert.equal(await trust.getTrust(accounts[0], accounts[3]), 1);
    assert.equal(await trust.getTrust(accounts[0], accounts[4]), -1);
    assert.equal(await trust.getTrust(accounts[0], accounts[5]), -1);
  });

  it("should issue trust score via a delegate", async () => {
    // accounts[1] is the identity
    // accounts[1] is also the owner
    // accounts[2] will become the delegate
    await didRegistry.addDelegate(
      accounts[1],
      web3.utils.fromAscii("DID-JWT"),
      accounts[2],
      86400 * 365,
      {
        from: accounts[1]
      }
    );
    await trust.issueTrustViaDelegate(accounts[1], accounts[3], 1, {
      from: accounts[2]
    });
    assert.equal(await trust.getTrust(accounts[1], accounts[3]), 1);
  });

  it("should return a list of users we issued trust on", async () => {
    await trust.issueTrust(accounts[2], 1);
    await trust.issueTrust(accounts[3], 1);
    await trust.issueTrust(accounts[4], -1);
    await trust.issueTrust(accounts[5], -1);

    await trust.issueTrust(accounts[3], -1, { from: accounts[2] });

    console.log(await trust.getTrusts(accounts[0]));
  });
});
