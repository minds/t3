# T3

Trust Tree Traversal

## Decentralized ID (DID)

T3 uses DID to build trust scores across your social world.

## Trust

Contract address: [0x619bebd9d803b4f0796c1dd0aece1e9fcadd55d1](https://rinkeby.etherscan.io/address/0x619bebd9d803b4f0796c1dd0aece1e9fcadd55d1)

## Inspiration
It's getting harder to know what's real and what's fake news. T3 provides trust scores that allow you to make an informed decision on whose posts you can believe, and whose you should be skeptical about.  Your own vote is only part of the trustworthiness calculation - we traverse 3 degrees of your social network and build a trust score based on your vote _and_ the votes of those you trust.

## What it does
There are three trust actions, Trust, Neutral, and Distrust. These actions are written to the blockchain and used to calculate trust scores. 

## How we built it
We created a simple angular site, a [Solidity smart contract](https://rinkeby.etherscan.io/address/0x619bebd9d803b4f0796c1dd0aece1e9fcadd55d1) to store the trust scores, the uPort protocol for authentication. In order to get enough identities in the system, we created a list of Simpsons characters, gave them some ETH, and created some trust relationships between them. Also built with caffeine, headphones & pizza.

## Challenges we ran into
Encountered a fundamental bug with uPort + Infura app - "legacy request rate exceeded", which ate a significant amount of time and made it hard to test and get started.  Also had issues with IFPS being too slow, which meant that the application couldn't use fully decentralized transaction signings and rely on delegates instead. 

## Accomplishments that we're proud of
We're on the blockchain! We delivered our fundamental goal of implementing a decentralized ID app. 

## What we learned
It's important to be flexible and continually re-assess goals and strategies on how to achieve them. The purple-haired twins on the Simpsons are named Sherri and Terri.

## What's next for T3: Trust Tree Traversal
Incorporate into Minds.com. More accurate trust scores. Performance enhancements and refactoring.

---- 

There are two trust actions, Trust and Distrust. These actions are written to the blockchain. Trust is given to entities. Entities can consist of either a 'owner' or 'artifact'.

## Registering

- uPort (https://developer.uport.me/guides/gettingstarted)
- Show immidiate tree of close connections in GUI
- Simple Trust & Distrust buttons

## Traversing the Trust Tree

1. Pick an entry point (default is self)
2. Return all entites from the entry point
3. nth degree should have been provided by application.

## Applications

- Minds Channels (eg. minds.com/mark)
- Minds Posts
- Twitter Posts via API
